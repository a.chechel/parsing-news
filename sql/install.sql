CREATE TABLE news (
    Id INTEGER PRIMARY KEY,
    Source VARCHAR(25) NOT NULL,
    LinkSource VARCHAR(255) NOT NULL,
    Title VARCHAR(200) NOT NULL,
    Description TEXT NOT NULL,
    DateTime TIMESTAMP NOT NULL,
    Category VARCHAR(150) NOT NULL,
    ImageUrl VARCHAR(255)
);