<?php

namespace Parsing;

use Parsing\Model\NewsRepository;

final class RBC extends AResource implements IResource {

    /**
     * @var int
     */
    private static int $countNews = 15;

    /**
     * @var string
     */
    private string $source = "RBC";

    /**
     * @var \Parsing\Entities\NewsModel
     */
    private Entities\NewsModel $singleNews;

    /**
     * @var string
     */
    protected string $sourceLink = "https://www.rbc.ru";

    /**
     * @var string
     */
    protected string $newsFeedList = "//div[@class='js-news-feed-list']/a";

    /**
     * @var string
     */
    protected string $title = "//span[contains(@class, 'news-feed__item__title')]";

    /**
     * @var string
     */
    protected string $dateTime = "//span[contains(@class, 'news-feed__item__date-text')]";

    /**
     * @var string
     */
    protected string $newsBody = "//div[contains(@class, 'article__content')]/div[contains(@class, 'article__text')]/p";

    /**
     * @var string
     */
    protected string $newsImage = "//div[contains(@class, 'article__main-image__wrap')]/img";

    /**
     * @throws \Exception
     */
    public function processed()
    {
        $this->getNews();

        $this->newsRepository->insertItems($this->newsArray);
    }

    /**
     * @return void
     * @throws \Exception
     */
    private function getNews(): void
    {
        $this->newsArray = new \ArrayObject();

        foreach ($this->parse() as $post)
        {
            if (self::$countNews-- == 0)
                break;

            $this->singleNews = new Entities\NewsModel();

            $dt = new \DateTime();

            $dt->format("YYYY-MM-DD");

            $this->singleNews->LinkSource = $post->href;

            $this->singleNews->Title = trim($post->find($this->title)->plaintext);

            $replaced = preg_replace("/[\xC2\xA0]/u", "", $post->find($this->dateTime)->plaintext);

            $split = preg_split("/[,]+/", $replaced);

            $this->singleNews->Category = trim($split[0]);

            $this->singleNews->DateTime = $split[1] ? new \DateTime($split[1]) : new \DateTime();

            $this->singleNews->Source = $this->source;

            $this->getSingleNews();

            $this->newsArray->append($this->singleNews);
        }
    }

    /**
     * @return void
     */
    private function getSingleNews()
    {
        $document = Document::fileGetHtml($this->singleNews->LinkSource);

        if ($image = $document->find($this->newsImage, 0))
            $this->singleNews->ImageUrl = $image->src;

        if ($body = $document->find($this->newsBody))
            $this->singleNews->Description = mb_convert_encoding((string)$body->innertext, 'UTF-8', 'HTML-ENTITIES');
    }
}