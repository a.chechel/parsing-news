<?php

namespace Parsing;

use Parsing\Model\NewsRepository;

abstract class AResource implements IResource {

    private Document $html;

    /**
     * @var \Parsing\Model\NewsRepository
     */
    protected NewsRepository $newsRepository;

    protected string $sourceLink;

    protected string $newsFeedList;

    protected string $title;

    protected string $dateTime;

    protected string $newsBody;

    protected string $newsImage;

    protected \ArrayObject $newsArray;

    public function __construct()
    {
        $this->html = Document::fileGetHtml($this->getSourceLink());

        $this->newsRepository = new NewsRepository();
    }

    /**
     * @return string
     */
    public function getSourceLink(): string
    {
        return $this->sourceLink;
    }

    /**
     * @return string
     */
    public function getSelector(): string
    {
        return $this->newsFeedList;
    }

    /**
     * @return null|\Parsing\Element|\Parsing\NodeList
     */
    public function parse()
    {
        return $this->html->find($this->getSelector());
    }

    /**
     * @return string|null
     */
    public function getJson(): ?string
    {
        return json_encode($this->newsRepository->getList()) ?? null;
    }
}