<?php

namespace Parsing\Entities;

class NewsModel {

    /**
     * @var int
     */
    public int $Id;

    /**
     * @var string
     */
    public string $Source;

    /**
     * @var string
     */
    public string $LinkSource;

    /**
     * @var string
     */
    public string $Title;

    /**
     * @var string
     */
    public string $Description = "";

    /**
     * @var string
     */
    public string $PreviewText = "";

    /**
     * @var \DateTime
     */
    public \DateTime $DateTime;

    /**
     * @var string
     */
    public string $Category;

    /**
     * @var null|string
     */
    public ?string $ImageUrl = "";
}