<?php

namespace Parsing;

class Element {

    /**
     * @var \DOMElement
     */
    protected $node;

    /**
     * Element constructor.
     *
     * @param \DOMNode $node
     */
    public function __construct(\DOMNode $node)
    {
        $this->node = $node;
    }

    /**
     * @return \DOMNode
     */
    public function getNode(): \DOMNode
    {
        return $this->node;
    }

    /**
     * @return Document
     */
    public function getDom(): Document
    {
        return new Document($this);
    }

    /**
     * @param string $selector
     * @param int|null $idX
     *
     * @return NodeList|Element|null
     */
    public function find(string $selector, ?int $idX = null)
    {
        return $this->getDom()->find($selector, $idX);
    }

    /**
     * @return string
     */
    public function html()
    {
        return $this->getDom()->html();
    }

    /**
     * @return string
     */
    public function innerHtml()
    {
        return $this->getDom()->innertext;
    }

    /**
     * @return string
     */
    public function text()
    {
        return $this->node->textContent;
    }

    /**
     * @param string $name
     *
     * @return string|null
     */
    public function getAttribute($name)
    {
        return $this->node->getAttribute($name);
    }

    /**
     * @param $name
     *
     * @return array|null|string
     */
    public function __get($name)
    {
        switch ($name)
        {
            case 'outertext':
                return $this->html();
            case 'innertext':
                return $this->innerHtml();
            case 'plaintext':
                return $this->text();
            case 'tag'      :
                return $this->node->nodeName;
            default         :
                return $this->getAttribute($name);
        }
    }
}