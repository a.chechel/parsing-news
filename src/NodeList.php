<?php

namespace Parsing;

class NodeList extends \ArrayObject {

    /**
     * @param string    $selector
     * @param int|null  $idX
     *
     * @return NodeList|Element|null
     */
    public function find(string $selector, ?int $idX = null)
    {
        $elements = new self();
        foreach ($this as $node)
        {
            foreach ($node->find($selector) as $res)
            {
                $elements->append($res);
            }
        }

        if (null === $idX)
            return $elements;


        if ($idX < 0)
            $idX = count($elements) + $idX;

        return isset($elements[$idX]) ? $elements[$idX] : null;
    }

    /**
     * @return string
     */
    public function text(): string
    {
        $text = '';

        foreach ($this as $node)
            $text .= $node->plaintext;

        return $text;
    }

    /**
     * @return string
     */
    public function innerHtml(): string
    {
        $text = '';
        foreach ($this as $node)
            $text .= $node->outertext;

        return $text;
    }

    /**
     * @param string $name
     *
     * @return string|null
     */
    public function __get(string $name): ?string
    {
        switch ($name)
        {
            case 'innertext':
                return $this->innerHtml();
            case 'plaintext':
                return $this->text();
        }

        return null;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->innerHtml();
    }

    /**
     * @param string    $selector
     * @param int|null  $idx
     *
     * @return Element|NodeList|null
     */
    public function __invoke(string $selector, ?int $idx = null)
    {
        return $this->find($selector, $idx);
    }
}