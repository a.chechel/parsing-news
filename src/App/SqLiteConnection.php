<?php

namespace Parsing\App;

class SqLiteConnection {
    /**
     * PDO instance
     * @var \PDO
     */
    private $pdo;

    /**
     * return in instance of the PDO object that connects to the SQLite database
     * @return \PDO
     */
    public function connect() {
        if ($this->pdo == null) {
            $this->pdo = new \PDO("sqlite:" . Config::PATH_TO_FILE);
        }
        return $this->pdo;
    }
}