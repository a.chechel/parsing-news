<?php

namespace Parsing;

class Document {
    /**
     * @var \DOMDocument
     */
    protected \DOMDocument $document;

    /**
     * @param string|\DOMDocument|Element|null $string An HTML or XML string or a file path
     */
    public function __construct($string = null)
    {
        if ($string instanceof \DOMDocument)
        {
            $this->document = $string;

            return;
        }

        $this->document = new \DOMDocument('1.0', 'UTF_8');

        $this->preserveWhiteSpace(false);

        if ($string instanceof Element)
        {
            $node = $string->getNode();

            $domNode = $this->document->importNode($node, true);
            $this->document->appendChild($domNode);

            return;
        }

        if ($string !== null)
        {
            $this->loadHtml($string);
        }
    }

    /**
     * @param bool $val
     *
     * @return $this
     */
    private function preserveWhiteSpace(bool $val = true): Document
    {
        $this->document->preserveWhiteSpace = $val;

        return $this;
    }

    /**
     * @param string $html
     *
     * @return Document
     * @throws \InvalidArgumentException if argument is not string
     */
    public function loadHtml(string $html): Document
    {
        $html = preg_replace('/>(?:[\t\s\n\r])+</', '><', $html);

        libxml_use_internal_errors(true);

        if (!$this->document->loadHTML($html, LIBXML_COMPACT|LIBXML_HTML_NOIMPLIED|LIBXML_HTML_NODEFDTD|LIBXML_NOENT|LIBXML_NOWARNING))
        {
            $this->document->loadHTML('<?xml encoding="UTF-8">' . $html, LIBXML_COMPACT|LIBXML_HTML_NOIMPLIED|LIBXML_HTML_NODEFDTD|LIBXML_NOENT|LIBXML_NOWARNING);
        }

        libxml_clear_errors();
        libxml_use_internal_errors(false);

        return $this;
    }

    /**
     * @param string $filePath
     *
     * @return Document
     * @throws \RuntimeException
     */
    public function loadHtmlFile(string $filePath): Document
    {
        if (!preg_match("/^https?:\/\//i", $filePath) && !file_exists($filePath))
            throw new \RuntimeException("File $filePath not exists");

        try
        {
            $html = DocumentLoad::getContent($filePath);
        }
        catch (\Exception $e)
        {
            throw new \RuntimeException("Could not load file $filePath");
        }

        if ($html === false)
        {
            throw new \RuntimeException("Could not load file $filePath");
        }

        return $this->loadHtml($html);
    }

    /**
     * Get dom node's
     *
     * @return string
     */
    public function html()
    {
        return trim($this->document->saveHTML());
    }

    /**
     * @param $name
     * @param $args
     *
     * @return bool|Document
     *
     * @throws \BadMethodCallException
     */
    public static function __callStatic($name, $args): Document
    {
        throw new \BadMethodCallException('Bad method call, does not exist');
    }

    /**
     * @param string $filePath
     *
     * @return \Parsing\Document
     */
    public static function fileGetHtml(string $filePath): Document
    {
        return (new Document())->loadHtmlFile($filePath);
    }

    /**
     * @param string $html
     *
     * @return \Parsing\Document
     */
    public static function strGetHtml(string $html): Document
    {
        return (new Document())->loadHtml($html);
    }

    /**
     * @param string $selector
     * @param int|null $idX
     *
     * @return mixed|\Parsing\Element|\Parsing\NodeList|null
     */
    public function find(string $selector, ?int $idX = null)
    {
        $nodesList = (new \DOMXPath($this->document))->query($selector);
        $elements = new NodeList();

        foreach ($nodesList as $node)
        {
            $elements[] = new Element($node);
        }

        if (null === $idX)
        {
            return $elements;
        }

        if ($idX < 0)
        {
            $idX = count($elements) + $idX;
        }

        return isset($elements[$idX]) ? $elements[$idX] : null;
    }
}