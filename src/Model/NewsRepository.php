<?php

namespace Parsing\Model;

use Parsing\App;
use Parsing\Entities;

class NewsRepository extends App\SqLiteConnection {

    /**
     * @var \PDO
     */
    private \PDO $pdo;

    /**
     * NewsRepository constructor.
     */
    public function __construct()
    {
        $this->pdo = $this->connect();
    }

    /**
     * @param \ArrayObject<Entities\NewsModel> $items
     */
    public function insertItems(\ArrayObject $items)
    {
        $this->pdo->beginTransaction();

        foreach ($items as $item)
        {
            $sql = "INSERT INTO news (Source, LinkSource, Title, Description, DateTime, Category, ImageUrl) 
                VALUES ('" . $item->Source . "', '" . $item->LinkSource . "',
                        '" . $item->Title . "', '" . $item->Description . "', 
                        '" . $item->DateTime->format('Y-m-d H:i:s') . "', '" . $item->Category . "', 
                        '" . $item->ImageUrl . "')";
            $this->pdo->query($sql);
        }

        $this->pdo->commit();
    }

    public function getList()
    {
        $stmt = $this->pdo->query("SELECT Id, Source, LinkSource, Title, Description, SUBSTRING(Description, 1, 197) as PreviewText, DateTime, Category, ImageUrl FROM news ORDER BY Id");

        $tables = new \ArrayObject();

        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC))
        {
            $nm = new Entities\NewsModel();

            $nm->Id = $row['Id'];

            $nm->Source = $row['Source'];

            $nm->LinkSource = $row['LinkSource'];

            $nm->Title = $row['Title'];

            $nm->Description = $row['Description'];

            $nm->PreviewText = strip_tags($row['PreviewText']) . "...";

            $nm->Category = $row['Category'];

            $nm->ImageUrl = $row['ImageUrl'];

            $nm->DateTime = new \DateTime($row['DateTime']);

            $tables[] = $nm;
        }

        return $tables;
    }
}