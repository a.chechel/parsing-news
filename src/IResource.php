<?php

namespace Parsing;

interface IResource {

    /**
     * @return string
     */
    function getSourceLink(): string;

    /**
     * @return string
     */
    function getSelector(): string;

    /**
     * @return mixed
     */
    function parse();
}