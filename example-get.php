<?php
require "vendor/autoload.php";

use \Parsing\RBC;
use \Parsing\App;

try
{
    $pdo = (new App\SqLiteConnection())->connect();
}
catch (\PDOException $e)
{
    echo $e->getMessage();
}

$resource = new RBC();

echo $resource->getJson();